import numpy as np
import tensorflow as tf
from collections import defaultdict
from modules.feature_generator import load_dataset
from modules.image_processor import evenly_rotate, random_rotate, is_polar_coordinate, crop_center


def image_augmentation(images, crop_width=64):
    rotated_images = random_rotate(images)
    if is_polar_coordinate(images):
        return rotated_images

    # cart_coordinate image need center cropping
    cropped_images = crop_center(rotated_images, crop_width)
    return cropped_images


def transfer_model_output_to_pred_dict(model_output):
    if type(model_output) is tuple:
        pred_dict = {
            'profile': model_output[0],
            'Vmax': model_output[1],
            'R34': model_output[2]
        }
    else:
        pred_dict = {
            'Vmax': model_output
        }
    return pred_dict


def calculate_loss_dict(model_output, loss_function, profile, Vmax, R34, Vmax_loss_sample_weight_exponent):
    pred_dict = transfer_model_output_to_pred_dict(model_output)
    loss_dict = {}
    if 'Vmax' in pred_dict:
        big_sample_weight = Vmax ** Vmax_loss_sample_weight_exponent
        normalized_sample_weight = big_sample_weight / tf.reduce_sum(big_sample_weight) * Vmax.shape[0]
        loss_dict['Vmax'] = loss_function(Vmax, pred_dict['Vmax'], sample_weight=normalized_sample_weight)

    if 'profile' in pred_dict:
        have_valid_profile = tf.cast(profile[:, 0] != -1, tf.float32)
        loss_dict['profile'] = loss_function(profile, pred_dict['profile'], sample_weight=have_valid_profile)

    if 'R34' in pred_dict:
        loss_dict['R34'] = loss_function(R34, pred_dict['R34'])
    return loss_dict


def get_sample_data(dataset, count):
    for batch_index, (images, feature, profile, Vmax, R34) in dataset.enumerate():
        valid_profile = profile[:, 0] != -999
        preprocessed_images = image_augmentation(images)
        sample_images = tf.boolean_mask(preprocessed_images, valid_profile)[:count, ...]
        sample_feature = tf.boolean_mask(feature, valid_profile)[:count, ...]
        sample_profile = tf.boolean_mask(profile, valid_profile)[:count, ...]
        sample_Vmax = tf.boolean_mask(Vmax, valid_profile)[:count, ...]
        sample_R34 = tf.boolean_mask(R34, valid_profile)[:count, ...]
        return sample_images, sample_feature, sample_profile, sample_Vmax, sample_R34


def blend_profiles(profiles):
    return sum(profiles)/len(profiles)


def rotation_blending(model, blending_num, images, feature):
    evenly_rotated_images = evenly_rotate(images, blending_num)
    pred_collection = defaultdict(list)
    for image in evenly_rotated_images:
        model_output = model(image, feature, training=False)
        pred_dict = transfer_model_output_to_pred_dict(model_output)
        for pred_type, pred_value in pred_dict.items():
            pred_collection[pred_type].append(pred_value)

    blended_preds = {}
    for pred_type, pred_list in pred_collection.items():
        if pred_type == 'profile':
            blended_preds[pred_type] = blend_profiles(pred_list)
        else:
            blended_preds[pred_type] = tf.reduce_mean(pred_list, 0)

    if 'profile' in blended_preds:
        return blended_preds['profile'], blended_preds['Vmax'], blended_preds['R34']
    else:
        return blended_preds['Vmax']


def apply_loss_ratio_to_losses(loss_dict, loss_ratio):
    for loss_type in loss_dict:
        loss_dict[loss_type] *= loss_ratio.get(loss_type, 0.0)
    return loss_dict


def evaluate_blending_loss(model, dataset, loss_function, blending_num=10):
    if loss_function == 'MSE':
        loss = tf.keras.losses.MeanSquaredError()
    elif loss_function == 'MAE':
        loss = tf.keras.losses.MeanAbsoluteError()

    avg_losses = defaultdict(lambda: tf.keras.metrics.Mean(dtype=tf.float32))

    for batch_index, (images, feature, profile, Vmax, R34) in dataset.enumerate():
        model_output = rotation_blending(model, blending_num, images, feature)
        pred_dict = transfer_model_output_to_pred_dict(model_output)

        for pred_type, pred_value in pred_dict.items():
            if pred_type == 'profile':
                batch_loss = loss(profile, pred_value)
            if pred_type == 'Vmax':
                batch_loss = loss(Vmax, pred_value)
            if pred_type == 'R34':
                batch_loss = loss(R34, pred_value)
            avg_losses[pred_type].update_state(batch_loss)

    blending_loss_dict = {
        loss_type: avg_loss.result()
        for loss_type, avg_loss in avg_losses.items()
    }

    return blending_loss_dict


def do_blending_evaluation_and_write_summary(epoch_index, summary_writer, model, datasets, loss_function, profiler_loss_ratio):
    # calculate blending loss
    total_loss = {}
    for phase in ['train', 'valid']:
        blending_loss_dict = evaluate_blending_loss(model, datasets[phase], loss_function)
        with summary_writer.as_default():
            for loss_type, loss_value in blending_loss_dict.items():
                tf.summary.scalar(f'[{phase}] blending_{loss_type}_loss', loss_value, step=epoch_index)
        if profiler_loss_ratio:
            blending_loss_dict = apply_loss_ratio_to_losses(blending_loss_dict, profiler_loss_ratio)
        total_loss[phase] = sum(blending_loss_dict.values())

    return total_loss['train'], total_loss['valid']


def upsampling_good_quality_VIS_data(is_good_quality_VIS):
    good_quality_count = tf.reduce_sum(is_good_quality_VIS)
    total_count = tf.reduce_sum(tf.ones_like(is_good_quality_VIS))
    good_quality_rate = good_quality_count / total_count
    sample_weight_after_upsampling = is_good_quality_VIS / good_quality_rate
    return sample_weight_after_upsampling


def replace_original_channel_with_generation(datasets, generator, replace_VIS=True, replace_PMW=True):

    def generate_new_images(generator, images, feature, replace_VIS, replace_PMW):
        generated_VIS, generated_PMW = generator(images, feature)
        IR1_WV = tf.gather(images, axis=-1, indices=[0, 1])
        VIS = generated_VIS if replace_VIS else tf.gather(images, axis=-1, indices=[2])
        PMW = generated_PMW if replace_PMW else tf.gather(images, axis=-1, indices=[3])
        new_images = tf.concat([IR1_WV, VIS, PMW], axis=-1)
        return new_images.numpy()

    def unpack_replace_repack(dataset, generator, replace_VIS, replace_PMW, mini_batch=3):
        prefetched_dataset = dataset
        batched_dataset = prefetched_dataset._input_dataset
        shuffled_dataset = batched_dataset._input_dataset
        zipped_dataset = shuffled_dataset._input_dataset
        images_dataset = zipped_dataset._datasets[0]
        feature_dataset = zipped_dataset._datasets[1]
        profile_dataset = zipped_dataset._datasets[2]
        Vmax_dataset = zipped_dataset._datasets[3]
        R34_dataset = zipped_dataset._datasets[4]

        prefetch_buffer = prefetched_dataset._buffer_size
        batch_size = batched_dataset._batch_size
        shuffle_buffer = shuffled_dataset._buffer_size

        new_images_batch_numpy_list = [
            generate_new_images(
                generator, images, feature,
                replace_VIS, replace_PMW
            )
            for images, feature in tf.data.Dataset.zip((images_dataset, feature_dataset)).batch(mini_batch)
        ]
        new_images_tensor = np.concatenate(new_images_batch_numpy_list)

        new_images_dataset = tf.data.Dataset.from_tensor_slices(new_images_tensor)
        new_zipped_dataset = tf.data.Dataset.zip((
            new_images_dataset, feature_dataset, profile_dataset, Vmax_dataset, R34_dataset
        ))
        new_shuffled_dataset = new_zipped_dataset.shuffle(shuffle_buffer)
        new_batched_dataset = new_shuffled_dataset.batch(batch_size)
        new_prefetched_dataset = new_batched_dataset.prefetch(prefetch_buffer)
        return new_prefetched_dataset

    new_datasets = {}
    for phase in datasets:
        new_datasets[phase] = unpack_replace_repack(datasets[phase], generator, replace_VIS, replace_PMW)

    del datasets
    return new_datasets


def get_tensorflow_datasets(data_folder, batch_size, shuffle_buffer, prefetch_buffer, good_VIS_only=False, valid_profile_only=False, coordinate='cart'):
    datasets = dict()
    for phase in ['train', 'valid', 'test']:
        phase_data = load_dataset(data_folder, phase, good_VIS_only, valid_profile_only, coordinate)
        images_tensor = phase_data['image']
        feature_tensor = phase_data['feature'].to_numpy(dtype='float32')
        profile_tensor = phase_data['profile']
        Vmax_tensor = phase_data['label'][['Vmax']].to_numpy(dtype='float32')
        R34_tensor = phase_data['label'][['R34']].to_numpy(dtype='float32')

        images_dataset = tf.data.Dataset.from_tensor_slices(images_tensor)
        feature_dataset = tf.data.Dataset.from_tensor_slices(feature_tensor)
        profile_dataset = tf.data.Dataset.from_tensor_slices(profile_tensor)
        Vmax_dataset = tf.data.Dataset.from_tensor_slices(Vmax_tensor)
        R34_dataset = tf.data.Dataset.from_tensor_slices(R34_tensor)

        zipped_dataset = tf.data.Dataset.zip((images_dataset, feature_dataset, profile_dataset, Vmax_dataset, R34_dataset))
        shuffled_dataset = zipped_dataset.shuffle(shuffle_buffer)
        batched_dataset = shuffled_dataset.batch(batch_size)
        prefetched_dataset = batched_dataset.prefetch(prefetch_buffer)

        datasets[phase] = prefetched_dataset

    return datasets

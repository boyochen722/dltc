import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import imageio
from modules.image_processor import is_polar_coordinate, polar2cart


def output_sample_generation(generator, sample_data, summary_writer, epoch_index, postfix):
    for phase in ['train', 'valid']:
        sample_images, sample_feature, sample_profile, sample_Vmax, sample_R34 = sample_data[phase]
        generated_VIS, generated_PMW = generator(sample_images, sample_feature)
        if is_polar_coordinate(generated_VIS):
            with summary_writer.as_default():
                tf.summary.image(
                    phase + '_VIS_polar_' + postfix,
                    generated_VIS,
                    step=epoch_index+1,
                    max_outputs=generated_VIS.shape[0]
                )
                tf.summary.image(
                    phase + '_PMW_polar_' + postfix,
                    generated_PMW,
                    step=epoch_index+1,
                    max_outputs=generated_PMW.shape[0]
                )
            generated_cart_VIS = tf.map_fn(lambda x: polar2cart(x), generated_VIS)
            generated_cart_PMW = tf.map_fn(lambda x: polar2cart(x), generated_PMW)
        else:
            generated_cart_VIS = generated_VIS
            generated_cart_PMW = generated_PMW

        with summary_writer.as_default():
            tf.summary.image(
                phase + '_VIS_cart_' + postfix,
                generated_cart_VIS,
                step=epoch_index+1,
                max_outputs=generated_cart_VIS.shape[0]
            )
            tf.summary.image(
                phase + '_PMW_cart_' + postfix,
                generated_cart_PMW,
                step=epoch_index+1,
                max_outputs=generated_cart_PMW.shape[0]
            )


def output_sample_profile_chart(profiler, sample_data, summary_writer, epoch_index):
    for phase, (sample_images, sample_feature, sample_profile, sample_Vmax, sample_R34) in sample_data.items():
        model_output = profiler(sample_images, sample_feature, training=False)
        if type(model_output) is not tuple:
            # will do nothing if the model is regressor but not profiler.
            return
        pred_profile, calculated_Vmax, calculated_R34 = model_output
        charts = []
        for i in range(10):
            charts.append(
                _draw_profile_chart(sample_profile[i], pred_profile[i], calculated_Vmax[i], calculated_R34[i])
            )
        chart_matrix = np.stack(charts).astype(np.int)
        with summary_writer.as_default():
            tf.summary.image(
                f'{phase}_profile_chart',
                chart_matrix.astype(np.float)/255,
                step=epoch_index,
                max_outputs=chart_matrix.shape[0]
            )


def _draw_profile_chart(profile, pred_profile, calculated_Vmax, calculated_R34):
    km = np.arange(0, 751, 5)
    plt.figure(figsize=(15, 10), linewidth=2)
    plt.plot(km, profile, color='r', label="profile")
    plt.plot(km, pred_profile, color='g', label="pred_profile")
    plt.axhline(y=calculated_Vmax, color='b', linestyle='-')
    plt.axvline(x=calculated_R34, color='y', linestyle='-')
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.ylim(0, 120)
    plt.xlabel("Radius", fontsize=30)
    plt.ylabel("Velocity", fontsize=30)
    plt.legend(loc="best", fontsize=20)
    plt.savefig('tmp.png')
    plt.close('all')
    RGB_matrix = imageio.imread('tmp.png')
    return RGB_matrix

FROM tensorflow/tensorflow:latest-gpu

EXPOSE 6090 6091

WORKDIR /app
ADD ./Pipfile /app/Pipfile
ADD ./run_server.sh /app/run_server.sh
ADD ./jupyter_config.py /root/.jupyter/jupyter_notebook_config.py

# default to use the first and second GPU
ENV CUDA_VISIBLE_DEVICES 0,1

# install python 3.8
RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt update
RUN apt install -y python3.8
RUN apt install -y tmux

# install pipenv
RUN pip install pipenv

# set up pipenv environment
RUN pipenv install --dev
RUN pipenv run pip install tensorflow
RUN pipenv run pip install tensorflow_addons

CMD pipenv run ./run_server.sh

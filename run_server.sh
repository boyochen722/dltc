#!/bin/sh

SESSION_NAME="servers"

# check if session exsist

tmux has-session -t ${SESSION_NAME} 2>/dev/null

if [ $? != 0 ]; then
    tmux new-session -s ${SESSION_NAME} -d
    tmux split-window -v -t ${SESSION_NAME}
    tmux send-keys -t ${SESSION_NAME}:0.0 'jupyter lab --allow-root --port 6090' C-m
    tmux send-keys -t ${SESSION_NAME}:0.1 'tensorboard --logdir logs --port 6091 --bind_all' C-m
fi

# keep bash alive until docker stop
trap : TERM INT; sleep infinity & wait
